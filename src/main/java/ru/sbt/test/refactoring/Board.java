package ru.sbt.test.refactoring;

import ru.sbt.test.refactoring.units.Unit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Board {
	private final int[][] fields;
	private final List<Unit> units;

	public Board(int[][] fields, List<Unit> units) {
		this.fields = fields;
		this.units = new ArrayList<>(units);
	}

	public int[][] getFields() {
		return fields;
	}

	public List<Unit> getUnits() {
		return units;
	}
}
