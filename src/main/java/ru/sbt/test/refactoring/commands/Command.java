package ru.sbt.test.refactoring.commands;

import ru.sbt.test.refactoring.Board;
import ru.sbt.test.refactoring.units.Unit;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public abstract class Command {
	private final Board board;

	public Command(Board board) {
		this.board = board;
	}

	public abstract void execute(final Unit unit);

	public Board getBoard() {
		return board;
	}
}
