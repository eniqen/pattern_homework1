package ru.sbt.test.refactoring.commands;

import ru.sbt.test.refactoring.Board;
import ru.sbt.test.refactoring.units.Unit;
import ru.sbt.test.refactoring.utils.CheckPositionUtil;

/**
 * Команда для движения вперед
 *
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class MoveCommand extends Command {

	public MoveCommand(Board board) {
		super(board);
	}

	@Override
	public void execute(Unit unit) {
		switch (unit.getOrientation()) {
			case NORTH:
				move(0, -1, unit);
				break;
			case EAST:
				move(-1, 0, unit);
				break;
			case SOUTH:
				move(0, 1, unit);
				break;
			case WEST:
				move(1, 0, unit);
				break;
		}
	}

	/**
	 * Всопомгательный метод хода
	 *
	 * @param dX   координата по которой делаем ход
	 * @param dY   координата по которой делаем ход
	 * @param unit юнит который ходит
	 */
	private void move(int dX, int dY, Unit unit) {
		unit.setPositionX(unit.getPositionX() + dX);
		unit.setPositionY(unit.getPositionY() + dY);

		CheckPositionUtil.checkBorders(unit.getPositionX(),
									   unit.getPositionY(),
									   getBoard().getFields());
	}
}
