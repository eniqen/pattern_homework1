package ru.sbt.test.refactoring.commands;

import ru.sbt.test.refactoring.Board;
import ru.sbt.test.refactoring.units.Orientation;
import ru.sbt.test.refactoring.units.Unit;

/**
 * Команда для поворота по часовой стрелке
 *
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class TurnClockWiseCommand extends Command {

	public TurnClockWiseCommand(Board board) {
		super(board);
	}

	@Override
	public void execute(Unit unit) {
		final Orientation unitOrientation = unit.getOrientation();
		unit.setOrientation(Orientation.turnRight(unitOrientation));
	}
}
