package ru.sbt.test.refactoring.units;

/**
 * Сторона для поворота
 */
public enum Orientation {
	NORTH("Север"),
	WEST("Восток"),
	SOUTH("Юг"),
	EAST("Запад");

	private final String description;

	Orientation(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * Разворот юнита направо
	 *
	 * @param orientation стартовая ориентация
	 * @return новое значение
	 */
	public static Orientation turnRight(Orientation orientation) {
		return values()[(orientation.ordinal() + 1) % values().length];
	}

	/**
	 * Поворачиваем влево
	 *
	 * @param orientation текущая ориентация юнита
	 * @return новое значение
	 */
	public static Orientation turnLeft(Orientation orientation) {
		return values()[(orientation.ordinal() - 1) % values().length];
	}

	/**
	 * Разворачиваем юнит обратно
	 *
	 * @param orientation стартовая ориентация
	 * @return ревернутое значение
	 */
	public static Orientation turnBack(Orientation orientation) {
		return values()[(orientation.ordinal() - 2) % values().length];
	}
}
