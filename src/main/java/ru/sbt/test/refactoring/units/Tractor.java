package ru.sbt.test.refactoring.units;

import ru.sbt.test.refactoring.commands.Command;

public class Tractor extends Unit {

	public Tractor(int x, int y, Orientation orientation) {
		super(x, y, orientation);
	}

	@Override
	public void move(Command command) {
		command.execute(this);
	}

	@Override
	public String getUnitName() {
		return "I'm Tractor";
	}
}