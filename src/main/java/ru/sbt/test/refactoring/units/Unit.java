package ru.sbt.test.refactoring.units;

import ru.sbt.test.refactoring.commands.Command;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public abstract class Unit {
	private int positionX;
	private int positionY;
	private Orientation orientation;

	public Unit() {
		this(0, 0, Orientation.EAST);
	}

	public Unit(int positionX, int positionY, Orientation orientation) {
		this.positionX = positionX;
		this.positionY = positionY;
		this.orientation = orientation;
	}

	/**
	 * Движение юнита по доске
	 * @param command команда определяющая как юнит будет двигаться
	 */
	public abstract void move(final Command command);

	/**
	 * Получение имени юнита
	 */
	public abstract String getUnitName();

	public int getPositionX() {
		return positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}
}
