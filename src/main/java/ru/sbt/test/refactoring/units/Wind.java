package ru.sbt.test.refactoring.units;

import ru.sbt.test.refactoring.commands.Command;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Wind extends Unit {

	@Override
	public void move(Command command) {
		command.execute(this);
	}

	@Override
	public String getUnitName() {
		return "I'm Wind";
	}
}
