package ru.sbt.test.refactoring.utils;

import ru.sbt.test.refactoring.exeptions.IncorrectPositionExeption;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public final class CheckPositionUtil {

	private CheckPositionUtil() {
	}

	/**
	 * проверка доски на возможность хода
	 *
	 * @param x     координата по горизонтали
	 * @param y     координата по вертикали
	 * @param board доска
	 */
	public static void checkBorders(int x, int y, int[][] board) {
		if (x < 0 || x >= board[0].length || y < 0 || y >= board.length) {
			throw new IncorrectPositionExeption();
		}
	}
}
