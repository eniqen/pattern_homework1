package ru.sbt.test.refactoring;

import junit.framework.TestCase;
import ru.sbt.test.refactoring.commands.MoveCommand;
import ru.sbt.test.refactoring.commands.TurnClockWiseCommand;
import ru.sbt.test.refactoring.exeptions.IncorrectPositionExeption;
import ru.sbt.test.refactoring.units.Orientation;
import ru.sbt.test.refactoring.units.Tractor;
import ru.sbt.test.refactoring.units.Unit;

import java.util.Collections;
import java.util.List;

import static ru.sbt.test.refactoring.units.Orientation.*;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class UnitTest extends TestCase {
	private static final int BOARD_X_SIZE = 10;
	private static final int BOARD_Y_SIZE = 10;
	private static final int[][] fields = new int[BOARD_X_SIZE][BOARD_Y_SIZE];
	private Board board = new Board(fields, Collections.singletonList(new Tractor(0, 0, WEST)));

	/**
	 * Проверяем что юнит умеет ходить вперед
	 */
	public void testShouldMoveForward() {
		List<Unit> units = board.getUnits();
		assertNotNull(units);
		Unit tractor = units.get(0);
		assertEquals(0, tractor.getPositionX());
		assertEquals(0, tractor.getPositionY());
		tractor.move(new MoveCommand(board));
		assertEquals(1, tractor.getPositionX());
		assertEquals(0, tractor.getPositionY());
	}

	/**
	 * Проверяем что юнит умеет поворачиваться
	 */
	public void testShouldTurn() {
		List<Unit> units = board.getUnits();
		assertNotNull(units);
		Unit tractor = units.get(0);
		TurnClockWiseCommand turnClockWiseCommand = new TurnClockWiseCommand(board);

		tractor.move(turnClockWiseCommand);
		assertEquals(SOUTH, tractor.getOrientation());
		tractor.move(turnClockWiseCommand);
		assertEquals(EAST, tractor.getOrientation());
		tractor.move(turnClockWiseCommand);
		assertEquals(NORTH, tractor.getOrientation());
		tractor.move(turnClockWiseCommand);
		assertEquals(WEST, tractor.getOrientation());
	}

	/**
	 * Делаем круг почета проверяем координаты
	 */
	public void testShouldTurnAndMoveInTheRightDirection() {
		List<Unit> units = board.getUnits();
		assertNotNull(units);
		Unit tractor = units.get(0);
		tractor.setOrientation(NORTH); //начнем с севера

		TurnClockWiseCommand turnClockWiseCommand = new TurnClockWiseCommand(board);
		MoveCommand moveCommand = new MoveCommand(board);

		tractor.move(turnClockWiseCommand);
		tractor.move(moveCommand);
		assertEquals(1, tractor.getPositionX());
		assertEquals(0, tractor.getPositionY());
		tractor.move(turnClockWiseCommand);
		tractor.move(moveCommand);
		assertEquals(1, tractor.getPositionX());
		assertEquals(1, tractor.getPositionY());
		tractor.move(turnClockWiseCommand);
		tractor.move(moveCommand);
		assertEquals(0, tractor.getPositionX());
		assertEquals(1, tractor.getPositionY());
		tractor.move(turnClockWiseCommand);
		tractor.move(moveCommand);
		assertEquals(0, tractor.getPositionX());
		assertEquals(0, tractor.getPositionY());
	}

	/**
	 * Проверка ошибки при выходе за границы доски
	 */
	public void testShouldTurnAndMoveInTheRightDirectionFail() {
		List<Unit> units = board.getUnits();
		assertNotNull(units);
		Unit tractor = units.get(0);

		MoveCommand moveCommand = new MoveCommand(board);

		tractor.setOrientation(Orientation.EAST);
		try {
			tractor.move(moveCommand);
			fail("Unit left the location of the board");
		} catch (IncorrectPositionExeption ex) {

		}
	}
}
